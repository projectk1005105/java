import React, { useState } from 'react';

const Forms1 = () => {
    const[data,setData] = useState({
        username:'',
        password:''
    })

    const {username,password} = data;

    const changeHandler = (e) => {
        setData({...data,[e.target.name]:e.target.value})
    }

    const submitHandler = (e) => {
        e.preventDefault();
        console.log(data.username)
        console.log(data.password)
    }

    return(
        <>
        <center>
           <form onSubmit={submitHandler}>
            <br/><br/>
           <input type='text' name='username' value={username} onChange={changeHandler} /><br/>
            <input type='text'name='password' value={password} onChange={changeHandler}/><br/>
            <input type='submit' name='submit' />
           </form>
        </center>
        </>
    )
}
export default Forms1;
