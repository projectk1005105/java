package lambdaexpressions;

interface Multiple {
	int mul(int a,int b);
}

public class Lambda2 {

	public static void main(String[] args) {
		
		Multiple m = (a,b) ->  a*b;
		
		System.out.println(m.mul(15, 10));

	}

}
