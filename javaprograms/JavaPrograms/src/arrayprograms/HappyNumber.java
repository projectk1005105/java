package arrayprograms;

import java.util.HashSet;

public class HappyNumber {

	public static void main(String[] args) {
		
		int n = 19;
		
		int result = happyNumber(n);
		
		if(result == 1){
			System.out.println(n +" is happy number");
		} else {
			System.out.println(n + " is not happy number");
		}
	}
	
	public static int happyNumber(int n){
		
		HashSet<Integer> hs = new HashSet<Integer>();
		
		while(true){
			int sum = 0;
			int r = 0;
			
			while(n >0){
				r = n%10;
				sum = sum + (r*r);
				n = n/10;
			}
			
			n = sum;
			
			if(sum == 1){
				return 1;
			}
			
			if(hs.contains(sum)){
				return -1;
			}
			else {
				hs.add(sum);
			}
		}
	}
}
