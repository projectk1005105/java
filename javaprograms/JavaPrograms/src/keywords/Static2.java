package keywords;

class Test5{
	static int number = 10;
	
	
	static void method(){
		number = 99;
		System.out.println("hiii");
		
	}
	
//	static void method(int a,int b){
//		System.out.println("a + b = " + a+b);
//	}
}

class Test6 extends Test5{
	
	
	static void method(){
		number = 19;
		System.out.println("hello");
	}
}

public class Static2 {

	public static void main(String[] args) {
		
		Test6.method();
		System.out.println(Test5.number);
		
	}

}
