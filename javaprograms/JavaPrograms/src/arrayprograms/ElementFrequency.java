package arrayprograms;

import java.util.Scanner;

public class ElementFrequency {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		System.out.println("ENter array length");
		int length = scan.nextInt();
		int[] arr = new int[length];
		System.out.println("enter elements");

		for (int i = 0; i < length; i++) {
			arr[i] = scan.nextInt();
		}
		for (int i = 0; i < length; i++) {
			System.out.print(arr[i]+" ");
		}
		System.out.println();
		
		for(int i=0;i<arr.length;i++){
			int count = 1;
			for(int j=i+1;j<arr.length;j++){
				
				if(arr[i] == arr[j]){
					count++;
					arr[j] = 0;
				}
			}
			if(arr[i] > 0){
				System.out.println(arr[i] + " : "+count);
			}
		}
		
	}
}
