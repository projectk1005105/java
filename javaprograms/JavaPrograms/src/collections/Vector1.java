package collections;

import java.util.*;

public class Vector1 {

	public static void main(String[] args) {
		
		Vector<Integer> v = new Vector<Integer>();
		v.add(6);
		v.add(3);
		v.add(1);
		v.add(2);
		v.add(4);
		v.add(10);
		
		System.out.println(v);
		System.out.println(v.remove(3));
		Collections.sort(v);
		System.out.println(v);
		System.out.println(v.contains(2));
		v.set(3,22);
		Collections.reverse(v);
		
		
		System.out.println(v);
		
	}

}
