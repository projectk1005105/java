package arrayprograms;

import java.util.Arrays;

public class EvenOddPositionSum {

	public static void main(String[] args) {

		int arr[] = { 1, 7, 9, 4, 3 };

		int[] arr1 = new int[arr.length / 2 + 1];
		int[] arr2 = new int[arr.length / 2 + 1];

		int even = 0;
		int odd = 0;

		for (int i = 0; i < arr.length; i += 2) {

			arr1[even] = arr[i];
			even++;
		}

		Arrays.sort(arr1);

		for (int i = 1; i < arr.length; i += 2) {

			arr2[odd] = arr[i];
			odd++;
		}

		Arrays.sort(arr2);
		int count = 0;
		for (int i = 0; i < arr2.length; i++) {
			if (arr2[i] > 0) {
				count++;
			}
		}

		int[] arr3 = new int[count];

		int a = 0;

		for (int i = 0; i < arr2.length; i++) {
			if (arr2[i] > 0) {
				arr3[a] = arr2[i];
				a++;
			}
		}
		
		int sum = arr1[arr1.length-2] + arr3[arr3.length-2];
		
		System.out.println("the sum is: " + sum);
	}

}
