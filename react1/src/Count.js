import React, { useState } from 'react';

const Count = () => {
    const [count, updateCount] = useState(1);
    const [inputValue, setInputValue] = useState('');

    const changeCount = () => {
        const p = parseInt(inputValue, 10);  // Convert input value to an integer
        if (!isNaN(p)) {  // Check if the value is a valid number
            updateCount(prevCount => prevCount + p);  // Add input value to the current count
        }
        setInputValue('');  // Clear the input field
    };

    return (
        <>
            <h1>Count: {count}</h1>
            <input 
                type="number" 
                value={inputValue} 
                onChange={(e) => setInputValue(e.target.value)} 
            /><br/>
            <button onClick={changeCount}>Click</button>
        </>
    );
};

export default Count;
