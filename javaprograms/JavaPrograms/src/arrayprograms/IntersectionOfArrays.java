package arrayprograms;

import java.util.ArrayList;

public class IntersectionOfArrays {

	public static void main(String[] args) {
		
		int arr1[] ={1,2,3,4,5,6,6};
		int arr2[] = {1,3,5,6,6,7,8,9};
		
		int result[] = intersectionOfArrays(arr1, arr2);
		for(int i=0;i<result.length;i++){
			System.out.print(result[i] +" ");
		}
		System.out.println();
	}
	
	public static int[] intersectionOfArrays(int[] arr1,int[] arr2){
		
		int[] view = new int[arr2.length];
		
		ArrayList<Integer> al = new ArrayList<>();
		
		for(int i=0;i<arr1.length;i++){
			for(int j=0;j<arr2.length;j++){
				if(arr1[i] == arr2[j] && view[j] == 0){
					al.add(arr1[i]);
					view[j] = 1;
				}
			}
		}
		
		int length = al.size();
		
		int[] arr3 = new int[length];
		
		for(int i=0;i<al.size();i++){
			arr3[i] = al.get(i);
		}
		
		return arr3;
	}

}
