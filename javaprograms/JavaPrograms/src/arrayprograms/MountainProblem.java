package arrayprograms;

public class MountainProblem {

	public static void main(String[] args) {
		
		int[] arr = {2,1,4,7,3,2,5};
		
		int result = maxMountain(arr);
		
		System.out.println(result);

	}
	
	public static int maxMountain(int arr[]){
		
		int n=arr.length;
		int maxMountainLength = 0;
		
		for(int i=1;i<n-1;){
			//int count = 0;
			if(arr[i]>arr[i-1] && arr[i]>arr[i+1]){
				int left = i-1;
				int right = i +1;
				
				while(left>0 && arr[left]>arr[left-1]){
					left--;
					System.out.print(left+" ");
					
				}
				System.out.println();
				
				while(right<n-1 && arr[right]>arr[right+1]){
					right++;
					System.out.print(right+" ");
					
				}
				  int mountainLength = right - left + 1;

		            maxMountainLength = Math.max(maxMountainLength, mountainLength);

		            i = right;
			} else {
				i++;
			}
		}
			return maxMountainLength;	
	}
}
