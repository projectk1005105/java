package stringprograms;

public class RemoveSpecialSymbols {

	public static void main(String[] args) {
		
		String str = "up@e@n%&*dra!";
		
		 str = str.replaceAll("[^a-zA-Z0-9]","");
		 str = str.replace('a', 'A');
		 
		System.out.println(str);
		
//		--------------------------------------------------------;
		
		String str1  = " u p e  nd  r  a";
		
		str1 = str1.replaceAll(" ", "");
		System.out.println(str1);
		
//		-------------------------------------------------------
		
		String str2  = "u p e nd  r  a";
		
		str2 = str2.replaceAll("\\s", "-");
		
		System.out.println(str2);
		
//		----------------------------------------------------------
		
        String str3  = "u p e nd  r  a";
		
		str3 = str3.replaceAll("\\s+", "-");
		
		System.out.println(str3);

	}

}
