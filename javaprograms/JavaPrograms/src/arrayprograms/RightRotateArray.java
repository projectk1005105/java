package arrayprograms;

public class RightRotateArray {

	public static void main(String[] args) {
		
		int arr[] = {1,2,3,4,5,6,7,8,0};
		int d = 5;
		for(int i=0;i<arr.length;i++){
		System.out.print(arr[i]+" ");
	}
	System.out.println();
		int[] result = rightRotate(arr,d);
		
		for(int i=0;i<result.length;i++){
			System.out.print(result[i]+" ");
		}
	}
	
	public static int[] rightRotate(int[] arr,int d){
		
		int[] arr2 = new int[d];
		int l=arr.length;
		
		for(int i=0;i<d;i++){
			arr2[i] = arr[l-d+i];
		}
		
		for(int i=0;i<l-d;i++){
			arr[l-1-i] = arr[l-1-d-i];
		}
		
		for(int i=0;i<d;i++){
			arr[i] = arr2[i];
		}
		
		return arr;
	}

}
