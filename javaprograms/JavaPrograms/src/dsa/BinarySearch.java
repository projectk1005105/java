package dsa;

public class BinarySearch {

	public static void main(String[] args) {
		
		int arr[] = {2,3,4,5,6,7,8};
		int target = 73;
		
		int result = binarySearch(arr,target);
		
		if(result == 1){
			System.out.println("number found");
		} else {
			System.out.println("Number not found");
		}
		
	}
	
	public static int binarySearch(int arr[], int target){
		
		int l = 0;
		int r = arr.length-1;
		int mid = 0;
		
		while(l<=r){
			mid = (l+r)/2;
			
			if(arr[mid] == target){
				return 1;
			} else if (arr[mid]<target){
				l = mid + 1;
			} else {
				l = mid - 1;
			}
		}
		
		return -1;

	}

}
