package arrayprograms;

public class MissingNumber {

	public static void main(String[] args) {
		
		int arr[] = {1,2,3,4,5,6,7,9};
		
		int result = findNumber(arr);
		
		System.out.println(result);

	}
	
	public static int findNumber(int[] arr){
		
		for(int i=0;i<arr.length-1;i++){
			if(arr[i+1]-arr[i] > 1){
				return arr[i]+1;
			}
		}
		return 0;
	}

}
