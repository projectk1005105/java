package keywords;

public class MethodOverLoading1 {
	
	public int sum(int a,int b){
	return a+b;
	}
	public float sum(float a,float b){
		return a+b;
	}

	public static void main(String[] args) {
		
		MethodOverLoading1 m = new MethodOverLoading1();

		System.out.println(m.sum(10, 20));
		System.out.println(m.sum(10f, 30f));
	}
}
