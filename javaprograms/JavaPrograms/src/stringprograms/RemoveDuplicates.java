package stringprograms;

import java.util.ArrayList;
import java.util.HashSet;

public class RemoveDuplicates {

	public static void main(String[] args) {
		
		String str = "aakbbbcgcc";
		StringBuilder sb = new StringBuilder();
		
		HashSet<Character> hs = new HashSet<>();
		
		for(int i=0;i<str.length();i++){
			hs.add(str.charAt(i));
		}
		
		for(char c : hs){
			sb.append(c);
		}
		
		System.out.println(sb);
		
//		ArrayList<Character> al = new ArrayList<Character>();
//		
//		for(int i=0;i<str.length();i++){
//			if(!al.contains(str.charAt(i))){
//				al.add(str.charAt(i));
//			}
//		}
//		
//		char[] ch = new char[al.size()];
//		
//		for(int i=0;i<ch.length;i++){
//			ch[i] = al.get(i);
//		}
//		
//		String str1 = new String(ch);
//		
//		System.out.println(str1);

	}
}
