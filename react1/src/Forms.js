import React, { useRef, useState } from 'react'

const Form = () => {

    const [userName,setName] = useState("");
    const pwd = useRef();

    const handler = (e) => {
        console.log(userName);
        console.log(pwd.current.value);
    }

    return(
        <>
        <center>
        <input type='text' value={userName} name="username" onChange={(e) => {
            setName(e.target.value)
        }} /><br/>
        <input type='password' ref={pwd} /><br/>
        <button onClick={handler}>Click</button>
        </center>
        </>
    )

}

export default Form;