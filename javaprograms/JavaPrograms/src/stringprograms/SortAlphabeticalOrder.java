package stringprograms;

import java.util.Arrays;

public class SortAlphabeticalOrder {

	public static void main(String[] args) {
		String str = "upendra";

		char[] ch = new char[str.length()];

		for (int i = 0; i < str.length(); i++) {
			ch[i] = str.charAt(i);
		}

		Arrays.sort(ch);

		System.out.println(new String(ch));

	}

}
