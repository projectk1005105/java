import React, { useState } from 'react'
import axios from 'axios'
import { useEffect } from 'react'

function Axioss() {
    const[products,setProducts] = useState([])
    useEffect(
        ()=>{
            getProducts()
        },[]
    )

   async function getProducts(){
        let p = await axios.get('https://fakestoreapi.com/products')

        console.log(p.data)
        setProducts(p.data)

    }
    return(
        <>
        <h1>Featching data...</h1>
        <ul>
                {products.map((product) => (
                    <li key={product.id}>  
                        <h2>{product.title}</h2>
                        <p>ID: {product.id}</p>
                        <p>Category: {product.category}</p>
                        <p>Price: ${product.price}</p>
                    </li>
                ))}
            </ul>
        </>
    )
}
export default Axioss
