package arrayprograms;

import java.util.ArrayList;

public class UnionOfTwoArrays {

	public static void main(String[] args) {
		
		int arr1[] ={1,2,3,4,5,6};
		int arr2[] = {1,3,5,6,7,8,9};
		
		int result[] = unionOfTwoArrays(arr1, arr2);
		for(int i=0;i<result.length;i++){
			System.out.print(result[i] +" ");
		}
		System.out.println();

	}
	
	public static int[] unionOfTwoArrays(int[] arr1,int arr2[]){
		
		ArrayList<Integer> al = new ArrayList<>();
		
		for(int i=0;i<arr1.length;i++){
			al.add(arr1[i]);
		}
		
		for(int i=0;i<arr2.length;i++){
			if(!al.contains(arr2[i])){
				al.add(arr2[i]);
			}
		}
		int length = al.size();
		
		int[] arr3 = new int[length];
		
		for(int i=0;i<arr3.length;i++){
			arr3[i] = al.get(i);
		}
		return arr3;
	}
}
