package keywords;

class Test {
	void m1(){
		System.out.println("hiii");
	}
}

class Test2 extends Test{
	void m1(){
		System.out.println("hello");
	}
}

public class MethodOverRiding1 {

	public static void main(String[] args) {
		
		Test2 t = new Test2();
		t.m1();
	}

}
