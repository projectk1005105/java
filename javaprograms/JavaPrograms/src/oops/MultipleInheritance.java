package oops;

interface Shape {
    void shape1();
}

interface Color{
	void color1();
}

class Rectangle implements Shape,Color{
	
	public void shape1(){
		System.out.println("rectangle..");
	}
	public void color1(){
		System.out.println("Red..");
	}
}

public class MultipleInheritance {

    public static void main(String[] args) {
    	Rectangle s;
        s = new Rectangle();
        s.shape1();
        s.color1();
    }
}