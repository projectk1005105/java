package keywords;

public class Static1 {
	
	int id;
	String name;
	static String college= "SSIT"; 
	
	public void details(int id,String name){
		System.out.println(id+": "+ name+": "+college);
	}

	public static void main(String[] args) {
		
		Static1 t = new Static1();
		t.details(1, "upendra");
		t.details(2, "naveen");
	}
}

