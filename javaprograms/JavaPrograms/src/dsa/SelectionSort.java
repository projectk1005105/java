package dsa;

public class SelectionSort {

	public static void main(String[] args) {
		
		int arr[] = {2,8,3,5,7,6};
		
		int index = -1;
		int temp = 0;
		
		
		for(int i=0;i<arr.length-1;i++){
			index = i;
			for(int j=i+1;j<arr.length;j++){
			 	
				if(arr[index] > arr[j]){
					index = j;
				}
			}
			
			temp = arr[i];
			arr[i] = arr[index];
			arr[index] = temp;
		}
	
		
		System.out.println();
		
		for(int n : arr){
			System.out.print(n+" ");
		}

	}

}
