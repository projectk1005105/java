import { useRef, useState } from "react";

const Header1 = () => {
    
    const[name,updateName] = useState('upendra')
    let pname = useRef(null)

    return(

        <>
        <center>
            <h1>{name}</h1>
            <input type='text' ref={pname} />
            <button onClick={()=>{
                updateName(pname.current.value)
            }}>Click</button>

            {/* <h1>{name}</h1>
            <input type="text" name="name" id='name' /><br/>
            <button onClick={ () => {
                let p = document.getElementById('name').value 
                updateName(p)
            }}>Click</button> */}
        </center>
        </>
       
    )
}

export default Header1;
