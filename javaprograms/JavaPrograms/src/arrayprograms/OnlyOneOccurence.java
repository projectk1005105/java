package arrayprograms;

import java.util.HashMap;
import java.util.Map.Entry;

public class OnlyOneOccurence {

	public static void main(String[] args) {
		
		int arr[] = {1,1,2,3,3,4,4};
		
		HashMap<Integer,Integer> hm = new HashMap<>();
		
		for(int i=0;i<arr.length;i++){
			int n = arr[i];
			if(hm.containsKey(n)){
				hm.put(n,hm.get(n)+1);
			}
			else {
				hm.put(n, 1);
			}
		}
		for(Entry<Integer,Integer> key : hm.entrySet() ){
			if(key.getValue() == 1){
				System.out.println(key.getKey());
			}
		}
	}
}
