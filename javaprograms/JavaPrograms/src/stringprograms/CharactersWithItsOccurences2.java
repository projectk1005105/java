package stringprograms;

import java.util.HashMap;

public class CharactersWithItsOccurences2 {

	public static void main(String[] args) {

		String str = "abbacgcc";
		System.out.println(str);

		String result = charactersWithItsOccurences(str);
		System.out.println(result);

	}

	public static String charactersWithItsOccurences(String str) {

		StringBuilder sb = new StringBuilder();

		HashMap<Character, Integer> hm = new HashMap<>();

		for (int i = 0; i < str.length(); i++) {
			char ch = str.charAt(i);
			if (hm.containsKey(ch)) {
				hm.put(ch, hm.get(ch) + 1);
				sb = sb.append(String.valueOf(hm.get(ch)));
			} else {
				hm.put(ch, 1);
				sb = sb.append(String.valueOf(hm.get(ch)));
			}
		}

		return String.valueOf(sb);
	}
}
