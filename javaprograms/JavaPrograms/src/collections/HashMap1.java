package collections;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.TreeMap;

public class HashMap1 {

	public static void main(String[] args) {
		
		HashMap<Integer,String> hm = new HashMap<Integer,String>();
		
		hm.put(10, "upendra");
		hm.put(2, "naveen");
		hm.put(80, "lava");
		hm.put(4, "saran");
		
		//hm.remove(1);
		hm.replace(1, "upendra123");
		
		System.out.println(hm);
		
//		System.out.println(hm.get(1));
//		System.out.println(hm.containsValue("upendra12m3"));
//		hm.clear();
//		System.out.println(hm+"empty");
//		System.out.println(hm.keySet());
//		System.out.println(hm.entrySet());
		
		for(Entry<Integer, String> key : hm.entrySet()){
			System.out.println(key.getKey()+" => "+ key.getValue());
		}
		
		TreeMap<Integer,String> tm = new TreeMap<>(hm);
		System.out.println(tm);
		
		
		
	}
}
