package collections;

import java.util.Hashtable;

public class HashTable1 {

	public static void main(String[] args) {
		
		Hashtable<Integer,String> ht = new Hashtable<Integer,String>();
		
		ht.put(1, "good");
		ht.put(2, "night");
		ht.put(30, "k");
		ht.put(10, "upendra");
		
		System.out.println(ht);
		
		ht.replace(30,"kandukuri");
		System.out.println(ht);
		
	}
}
