package arrayprograms;

public class LeftRotateArray {

	public static void main(String[] args) {
		
		int arr[] = {1,2,3,4,5,6,7,8};
		int d = 3;
		for(int i=0;i<arr.length;i++){
		System.out.print(arr[i]+" ");
	}
	System.out.println();
		int[] result = leftRotate(arr,d);
		
		for(int i=0;i<result.length;i++){
			System.out.print(result[i]+" ");
		}
	}
	
	public static int[] leftRotate(int[] arr,int d){
		
		int[] arr2 = new int[d];

		int l = arr.length;

		for (int i = 0; i < d; i++) {
			arr2[i] = arr[i];
		}

		for (int i = 0; i < l - d; i++) {
			arr[i] = arr[i + d];
		}

		for (int i = 0; i < d; i++) {
			arr[l - d + i] = arr2[i];
		}
		return arr;
		
//		int n = arr.length;
//		int[] arr2 = new int[d];
//		
//		for(int i =0;i<d;i++){
//			arr2[i] = arr[i];
//			System.out.print(arr2[i]+" ");
//		}
//		System.out.println();
//		
//		for(int i=0;i<arr.length;i++){
//			System.out.print(arr[i]+" ");
//		}
//		System.out.println();
//		
//		
//		for(int i=d;i<arr.length;i++){
//			arr[i-d] = arr[i];
//			System.out.print(arr[i]+" ");
//		}
//		System.out.println();
//		
//		int j=0;
//		for(int i=n-d;i<n;i++){
//			arr[i] = arr2[j++];
//		}
//		return arr;
	}
}
