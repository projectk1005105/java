package oops;

class Test {
	private int id;
	private String name;
	private float salary;
	
	Test(){
		
	}
	
	public int getId(){
		return id;
	}
	public void setId(int id){
		this.id = id;
	}
	
	public String getName(){
		return name;
	}
	public void setName(String name){
		this.name = name;
	}
	
	public float getSalary(){
		return salary;
	}
	public void setSalary(float salary){
		this.salary = salary;
	}
	
}

public class Encapsulation1 {

	public static void main(String[] args) {
		
		Test t = new Test();
		t.setId(10);
		t.setName("Upendra");
		t.setSalary(1000);
		
		System.out.println(t.getId()+" : "+t.getName()+": "+t.getSalary());
	}

}
