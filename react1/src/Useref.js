
import React, { useRef, useState } from 'react';

function Useref(){
    let x = useRef(10)

    const [number,setNumber] = useState(10)

    let Z = 100

    let name = useRef('upendra')

    return(
        <>
        <center>

        <h1>X Value:{x.current}</h1>
        <h1>Z value: {Z}</h1>


        <button onClick={()=>{

            console.log(x)
            x.current = x.current+10

            console.log(x.current)
            setNumber(number+10)
        }}>Click</button>
        </center>


        </>

    )
}
export default Useref;