package collections;

import java.util.*;

public class LinkedList1 {

	public static void main(String[] args) {
		
		LinkedList<Integer> ll = new LinkedList<>();
		
		ll.add(1);
		ll.add(2);
		ll.add(30);
		ll.add(4);
		ll.add(5);
		
		System.out.println(ll);
//		System.out.println(ll.get(2));
//		System.out.println(ll.set(4, 99));
//		System.out.println(ll.remove(4));
//		System.out.println(ll.contains(5));
//		System.out.println(ll.isEmpty());


		Collections.sort(ll);

		Iterator<Integer> itr = ll.iterator();
		
		while(itr.hasNext()){
			System.out.println(itr.next());
		}

	}
}
