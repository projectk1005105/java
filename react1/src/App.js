
import './App.css';
import Axioss from './Axioss';
import Count from './Count';
import Forms from './Forms';
import Forms1 from './Forms1';
import Header from './Header';
import Header1 from './Header1';
import Map1 from './Map1';
import Onchange from './Onchange';
import Useeffect1 from './Useeffect1';
import Useref from './Useref';


function App() {
  return (
    <div className="App">
        {/* <Header></Header> */}
        {/* <Header1></Header1> */}
        {/* <Onchange></Onchange> */}
        {/* <Useeffect1></Useeffect1> */}
        {/* <Forms></Forms> */}
        {/* <Forms1></Forms1> */}
        {/* <Map1></Map1> */}
        {/* <Count></Count> */}
        {/* <Useref></Useref> */}
        <Axioss></Axioss>
    </div>
  );
}

export default App;
