package basicprograms;

import java.util.Scanner;

public class PrimeNumber {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter number to check prime number");
		int n = scan.nextInt();
		
		int count = 0;
		
		for(int i=2;i<n/2;i++){
			if(n%i == 0){
				count++;
				break;
			}
		}
		if(count >0){
			System.out.println(n+" is not a prime number");
		} else {
			System.out.println(n+ " is prime number");
		}
	}
}
