package basicprograms;

import java.util.Scanner;

public class PalindromeString {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter String");
		String str = scan.next();
		
		String str1 = " ";
		
		for(int i=str.length()-1;i>=0;i--){
			str1 += str.charAt(i);
		}
		
		if(str.equals(str1)){
			System.out.println(str+" is palindrome");
		} else {
			System.out.println(str+" is not palindrome");
		}
	}
}
