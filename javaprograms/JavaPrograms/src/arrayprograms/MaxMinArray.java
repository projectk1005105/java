package arrayprograms;

public class MaxMinArray {

	public static void main(String[] args) {
		
		int arr[] = {1,2,3,4,5,6,7,8};
		
		int[] result = maxMin(arr);

		for(int i=0;i<result.length;i++){
			System.out.print(result[i]+" ");
		}
	}
	
	public static int[] maxMin(int[] arr){
		int length = arr.length;
		int l1 = length/2;

		int l2 = length - l1;
		
		int[] arr1 = new int[l1];
		int[] arr2 = new int[l2];
		
		for(int i =0;i<l1;i++){
			arr1[i] = arr[i];
		}
		
		for(int i=0;i<l2;i++){
			arr2[i] = arr[length-1-i];
		}
		
		int arr3[] = new int[length];
		
		int k=0;
		int l =0;
		for(int i=0;i<arr3.length;i++){
			if(i % 2 == 0){
				arr3[i] = arr2[k++];
			} else {
				arr3[i] = arr1[l++];
			}
		}
		return arr3;
	}
}
