package arrayprograms;

import java.util.Arrays;
import java.util.Scanner;

public class SecondLargestElement {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("enter array length");
		
		int length = scan.nextInt();
		
		int[] arr = new int[length];
		
		System.out.println("enter array elements");
		for(int i=0;i<arr.length;i++){
			arr[i] = scan.nextInt();
		}
		
		int largest = arr[0];
		int secondlargest = -1;
		
		for(int i=1;i<arr.length;i++){
			if(arr[i]>largest){
				largest = arr[i];
			}
		}
		
		for(int i=0;i<arr.length;i++){
			if(arr[i]>secondlargest && arr[i] < largest){
				secondlargest = arr[i];
			}
		}
		
		System.out.println(secondlargest);
		
		Arrays.sort(arr);
		
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+" ");
		}
		
//		Arrays.sort(arr);
//		
//		for(int i=0;i<arr.length;i++){
//			System.out.print(arr[i]+" ");
//		}
//		System.out.println();
//		
//		for(int i=arr.length-1;i>0;i--){
//			if(arr[i-1]<arr[i]){
//				System.out.println(arr[i-1]);
////				break;
////			}
//		}
	}
}
