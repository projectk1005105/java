package stringprograms;

public class CharacterWithItsOccurence {

	public static void main(String[] args) {
		 String str = "opentext";
	       System.out.println(str);
	       char ch = 't';
	       int count = 1;
	       char[] arr = str.toCharArray();
	      
	       for(int i=0;i<arr.length;i++){
	           if(arr[i] == ch){
	                arr[i]  = String.valueOf(count).charAt(0);
	               count++;
	           }
	       }
	       System.out.println(new String(arr));

	}

}
