package stringprograms;

import java.util.Arrays;
import java.util.Scanner;

public class Anagram {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter 2 strings..");
		
		String s1 = scan.next();
		String s2 = scan.next();
		
		s1 = s1.toLowerCase();
		s2 = s2.toLowerCase();
		
		char[] ch = new char[s1.length()];
		for(int i=0;i<ch.length;i++){
			ch[i] = s1.charAt(i);
		}
		Arrays.sort(ch);
		
		String str1 = new String(ch);
		
		System.out.println(str1);
		
		char[] ch1 = new char[s2.length()];
		for(int i=0;i<ch1.length;i++){
			ch1[i] = s2.charAt(i);
		}
		Arrays.sort(ch1);
		
		String str2 = new String(ch1);
		
		System.out.println(str2);
		
		if(str1.equals(str2)){
			System.out.println("given strings are anagram");
		} else {
			System.out.println("given strings are not anagram");
		}
	}
}
