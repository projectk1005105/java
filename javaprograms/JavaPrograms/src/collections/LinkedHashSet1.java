package collections;

import java.util.LinkedHashSet;

public class LinkedHashSet1 {

	public static void main(String[] args) {
		
		LinkedHashSet<Integer> lhs = new LinkedHashSet<Integer>();
		lhs.add(3);
		lhs.add(1);
		lhs.add(10);
		lhs.add(7);
		lhs.add(5);
		
		System.out.println(lhs);
		
		for(int values : lhs){
			System.out.println(values);
		}
		

	}

}
