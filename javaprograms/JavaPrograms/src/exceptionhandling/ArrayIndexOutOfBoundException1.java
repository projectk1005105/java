package exceptionhandling;

public class ArrayIndexOutOfBoundException1 {

	public static void main(String[] args) {
		
		int arr[] = {1,2,3,4,5};
		
		try{
		System.out.println(arr[16]);
		}
		catch(ArrayIndexOutOfBoundsException e){
			//System.out.println(e.toString());
			e.printStackTrace();
		}
	}

}
