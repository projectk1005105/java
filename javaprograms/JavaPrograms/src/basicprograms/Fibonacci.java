package basicprograms;

import java.util.Scanner;

public class Fibonacci {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter number");
		int n = scan.nextInt();
		
		int num1 = 0;
		int num2 = 1;
		
		System.out.print(num1 +","+num2);
		
		int temp=0;
		
		for(int i=1;i<=n;i++){
			temp = num1;
			num1 = num2;
			num2 = num1 + temp;
			System.out.print(","+num2);
		}
	}
}
