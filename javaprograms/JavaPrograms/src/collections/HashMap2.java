package collections;

import java.util.HashMap;
import java.util.Map.Entry;

public class HashMap2 {

	public static void main(String[] args) {
		
		String str = "upendrakandukuri";
		
		HashMap<Character,Integer> hm = new HashMap<>();
		
		for(int i =0;i<str.length();i++){
			char ch = str.charAt(i);
			
			if(hm.containsKey(ch)){
				int count = hm.get(ch) + 1;
				hm.put(ch, count);
			} else {
				hm.put(ch, 1);
			}
		}
		
		for(Entry<Character, Integer> key : hm.entrySet()){
			System.out.println(key.getKey()+" => "+ key.getValue());
		}
		
	}

}
