package collections;

import java.util.HashMap;
import java.util.TreeMap;

public class HashMap3 {

	public static void main(String[] args) {
		
		
		String str = "upendra goodmorning";
		
		HashMap<Character,Integer> hm = new HashMap();
		
		for(int i=0;i<str.length();i++){
			 char ch = str.charAt(i);
			 
			 if(hm.containsKey(ch)){
				 hm.put(ch,hm.get(ch)+1);
			 } else {
				 hm.put(ch,1);
			 }
		}
		
		 System.out.println("HashMap: " + hm);

	        TreeMap<Character, Integer> tm = new TreeMap<>(hm);

	        System.out.println("TreeMap: " + tm);

	        for (char key : tm.keySet()) {
	            System.out.println(key + " = " + tm.get(key));
	        }
	        

	}

}
