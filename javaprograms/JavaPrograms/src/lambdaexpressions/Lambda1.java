package lambdaexpressions;

interface Add {
	int sum(int a,int b);
}

class Demo implements Add {
	
	public int sum (int a,int b){
		return a+b;
	}
}

public class Lambda1 {

	public static void main(String[] args) {
		
//		Demo d = new Demo();
//		System.out.println(d.sum(10,20));
		
		Add d = (a,b)->{
			return a+b;
		};
		System.out.println(d.sum(10, 20));

	}

}
