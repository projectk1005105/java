import React, { useState } from 'react'

const Onchange = () => {

    const [name,updateName] = useState("");

    const onChange = e => {
        updateName({[e.target.name]:[e.target.value]})
    }

    const submitHandler = e => {
        e.preventDefault();
        console.log(name);
    }

  return (
    <div>
        <form onSubmit={submitHandler}>
        <input type='text' value={name} name='name' onChange={onChange} />

        <input type='submit' name='submit' />
        </form>
      
    </div>
  )
}

export default Onchange
