package stringprograms;

public class StringWithSpaceReverse {

	public static void main(String[] args) {
		
		System.out.println(reserverString("Hello World"));
	}
	
	public static  String reverse(String str){
		String reverse = "";
		for(int i=str.length()-1;i>=0;i--){
			reverse += str.charAt(i);
		}
		return reverse;
	}
	
	public static String reserverString(String str){
		String[] arr = str.split(" ");
		
		for(int i =0;i<arr.length;i++){
			arr[i] = reverse(arr[i]);
		}
		
		String str1 = String.join(" ", arr);
		
		return str1;
	}

}
