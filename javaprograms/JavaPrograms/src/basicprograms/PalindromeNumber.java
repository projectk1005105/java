package basicprograms;

import java.util.Scanner;

public class PalindromeNumber {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter number to check palindrome or not");
		int n = scan.nextInt();
		
		int r = n;
		int temp;
		
		int sum = 0;
	
		while(r>0){
			temp = r%10;
			sum = sum *10 + temp;
			r = r/10;
		}
		
		System.out.println(sum);
		
		if(sum == n){
			System.out.println(n+ " is palindrome");
		} else {
			System.out.println(n+" is not palindrome");
		}
	}

}
